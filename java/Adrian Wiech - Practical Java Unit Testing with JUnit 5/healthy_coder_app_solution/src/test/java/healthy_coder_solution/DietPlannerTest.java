package healthy_coder_solution;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DietPlannerTest {

  private DietPlanner dietPlanner;

  @BeforeEach
  void setUp() {
    dietPlanner = new DietPlanner(20, 30, 50);
  }

  @AfterEach
  void tearDown() {

  }

  @Test
  @DisplayName("Should return correct diet plan when correct coder.")
  void shouldReturnCorrectDietPlanWhenCorrectCoder() {
    // Given
    Coder coder = new Coder(1.82, 75.0, 26, Gender.MALE);

    DietPlan expected = new DietPlan(2202, 110, 73, 275);

    // When
    DietPlan actual = dietPlanner.calculateDiet(coder);

    // Then
    assertAll(
        () -> assertEquals(expected.getCalories() , actual.getCalories()),
        () -> assertEquals(expected.getProtein() , actual.getProtein()),
        () -> assertEquals(expected.getFat() , actual.getFat()),
        () -> assertEquals(expected.getCarbohydrate() , actual.getCarbohydrate())
    );
  }


  @RepeatedTest(value = 10, name = RepeatedTest.LONG_DISPLAY_NAME)
//  @DisplayName("Should return correct diet plan when correct coder.")
  void shouldReturnCorrectDietPlanWhenCorrectCoderRepeat() {
    // Given
    Coder coder = new Coder(1.82, 75.0, 26, Gender.MALE);

    DietPlan expected = new DietPlan(2202, 110, 73, 275);

    // When
    DietPlan actual = dietPlanner.calculateDiet(coder);

    // Then
    assertAll(
        () -> assertEquals(expected.getCalories() , actual.getCalories()),
        () -> assertEquals(expected.getProtein() , actual.getProtein()),
        () -> assertEquals(expected.getFat() , actual.getFat()),
        () -> assertEquals(expected.getCarbohydrate() , actual.getCarbohydrate())
    );
  }
}