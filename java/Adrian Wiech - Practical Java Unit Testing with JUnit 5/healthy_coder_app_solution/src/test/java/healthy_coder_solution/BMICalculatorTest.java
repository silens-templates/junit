package healthy_coder_solution;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

@ExtendWith(MockitoExtension.class)
class BMICalculatorTest {

  private String environment = "dev";

  @BeforeAll
  static void beforeAll(){
    // Code too expensive to run before each test.
  }

  @AfterAll
  static void afterAll(){
    // shut down server, databases,  websockets, etc...
  }


  @Test
  @DisplayName("Should return true when diet is recommended.")
  void shouldReturnTrueWhenDietRecommended() {// Use 'Should' 'When' naming convention.
    // Given
    double weight = 89.0;
    double height = 1.72;

    // When
    boolean recommended = BMICalculator.isDietRecommended(weight, height);

    // Then
    assertTrue(recommended);
  }

  @Test
  @DisplayName("Should return false when diet is not recommended.")
  void shouldReturnFalseWhenDietNotRecommended() {// Use 'Should' 'When' naming convention.
    // Given
    double weight = 50.0;
    double height = 1.82;

    // When
    boolean recommended = BMICalculator.isDietRecommended(weight, height);

    // Then
    assertFalse(recommended);
  }

  @Test
  @DisplayName("Should throw an arithmetic exception when height is zero")
  void shouldThrowArithmeticExceptionWhenHeightZero() {
    // Given
    double weight = 50.0;
    double height = 0.0;

    // When
    Executable executable = () -> BMICalculator.isDietRecommended(weight, height);

    // Then
    assertThrows(ArithmeticException.class, executable);
  }

  @Test
  @DisplayName("Should return coder with worse BMI coder when coder list is not empty.")
  void shouldReturnCoderWithWorseBmiCoderWhenCoderListNotEmpty() {
    // Given
    List<Coder> coders = new ArrayList<>();
    coders.add(new Coder(1.80, 60.0));
    coders.add(new Coder(1.82, 98.0));
    coders.add(new Coder(1.82, 64.7));

    // When
    Coder coderWorstBmi = BMICalculator.findCoderWithWorstBMI(coders);

    // Then
    assertAll(
        () -> assertEquals(1.82, coderWorstBmi.getHeight()),
        () -> assertEquals(98.0, coderWorstBmi.getWeight())
    );
  }

  @Test
  @DisplayName("Should return null with worse BMI coder when coder list is empty.")
  void shouldReturnNullWithWorseBmiCoderWhenCoderListEmpty() {
    // Given
    List<Coder> coders = new ArrayList<>();

    // When
    Coder coderWorstBmi = BMICalculator.findCoderWithWorstBMI(coders);

    // Then
    assertNull(coderWorstBmi);
  }

  @Test
  @DisplayName("Should return correct BMI score array when coder list is not empty.")
  void shouldReturnCorrectBmiScoreArrayWhenCoderListNotEmpty() {
    // Given
    List<Coder> coders = new ArrayList<>();
    coders.add(new Coder(1.80, 60.0));
    coders.add(new Coder(1.82, 98.0));
    coders.add(new Coder(1.82, 64.7));

    double[] expected = {18.52, 29.59, 19.53};

    // When
    double[] bmiScores = BMICalculator.getBMIScores(coders);

    // Then
    assertArrayEquals(expected, bmiScores);
  }

  @ParameterizedTest
  @ValueSource(doubles = {89.0, 95.0, 110.0})
  @DisplayName("Should return true when diet is recommended.")
  void shouldReturnTrueWhenDietRecommendedParameterized(Double coderWeight) {// Use 'Should' 'When' naming convention.
    // Given
    double weight = coderWeight;
    double height = 1.72;

    // When
    boolean recommended = BMICalculator.isDietRecommended(weight, height);

    // Then
    assertTrue(recommended);
  }

  @ParameterizedTest(name = "weight={0}, height={1}")
  @CsvSource(value = {"89.0, 1.72", "95.0, 1.75", "110.0, 1.78"})
  @DisplayName("Should return true when diet is recommended.")
  void shouldReturnTrueWhenDietRecommendedCsvParameterized(Double coderWeight, Double coderHeight) {// Use 'Should' 'When' naming convention.
    // Given
    double weight = coderWeight;
    double height = coderHeight;

    // When
    boolean recommended = BMICalculator.isDietRecommended(weight, height);

    // Then
    assertTrue(recommended);
  }

  @ParameterizedTest(name = "weight={0}, height={1}")
  @CsvFileSource(resources = "/diet-recommended-input-data.csv", numLinesToSkip = 1)
  @DisplayName("Should return true when diet is recommended.")
  void shouldReturnTrueWhenDietRecommendedCsvFileParameterized(Double coderWeight, Double coderHeight) {// Use 'Should' 'When' naming convention.
    // Given
    double weight = coderWeight;
    double height = coderHeight;

    // When
    boolean recommended = BMICalculator.isDietRecommended(weight, height);

    // Then
    assertTrue(recommended);
  }

  @Test
  void should_ReturnCoderWithWorstBmiIn1Ms_When_CoderListHas10000Elements(){
    // Given
    List<Coder> coders = new ArrayList<>();
    for(int i=0; i <1000;  i++){
      coders.add(new Coder(1.0 + i, 10.0 + i));
    }

    // When
    Executable executable = () -> BMICalculator.findCoderWithWorstBMI(coders);

    // Then
    assertTimeout(Duration.ofMillis(500), executable);
  }

  @Test
  void should_ReturnCoderWithWorstBmiIn1Ms_When_CoderListHas10000ElementsAssumptionTest(){
    // Given
    assumeTrue(this.environment.equals("prod")); // exit when false

    List<Coder> coders = new ArrayList<>();
    for(int i=0; i <1000;  i++){
      coders.add(new Coder(1.0 + i, 10.0 + i));
    }

    // When
    Executable executable = () -> BMICalculator.findCoderWithWorstBMI(coders);

    // Then
    assertTimeout(Duration.ofMillis(500), executable);
  }
}