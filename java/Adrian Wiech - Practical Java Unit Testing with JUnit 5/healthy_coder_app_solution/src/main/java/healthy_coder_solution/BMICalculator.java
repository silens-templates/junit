package healthy_coder_solution;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Comparator;
import java.util.List;

public class BMICalculator {
	
	private static final double BMI_THRESHOLD = 25.0;

	public static boolean isDietRecommended(double weight, double height) {
		if (height == 0.0) {
			throw new ArithmeticException();
		}

		double bmi = weight / (height * height);
		if (bmi < BMI_THRESHOLD)
			return false;
		return true;
	}

	public static Coder findCoderWithWorstBMI(List<Coder> coders) {
		return coders.stream().sorted(Comparator.comparing(BMICalculator::calculateBMI))
				.reduce((first, second) -> second).orElse(null);
	}

	public static double[] getBMIScores(List<Coder> coders) {
		double[] bmiScores = new double[coders.size()];
		for (int i = 0; i < bmiScores.length; i++) {
			bmiScores[i] = BMICalculator.calculateBMI(coders.get(i));
		}
		return bmiScores;
	}

	private static double calculateBMI(Coder coder) {
		double height = coder.getHeight();
		double weight = coder.getWeight();
		if (height == 0.0)
			throw new ArithmeticException();
		double bmi = weight / (height * height);
		return Math.round(bmi * 100) / 100.0;
	}


	public static void main(String[] args) {
//    File file = null;

		BufferedWriter bufferedWriter = null;

		FileWriter fileWriter = null;
		try {
			File file = new File("D:/meanLobTwoAngles.csv");

			fileWriter = new FileWriter(file);

			bufferedWriter = new BufferedWriter(fileWriter);

			String data = "TechBlogStation";

			StringBuilder stringBuilder = new StringBuilder();

			for (int i = 0; i < 360; i++){
				for (int j = 0; j < 360; j++){
					stringBuilder.append(i);
					stringBuilder.append(",");
					stringBuilder.append(j);
					stringBuilder.append(",");
					stringBuilder.append(averageAngle(i, j));
					stringBuilder.append("\n");
				}
			}

			bufferedWriter.write(stringBuilder.toString());

			bufferedWriter.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (bufferedWriter != null) {
					bufferedWriter.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static double averageAngle(int i, int j){
		return ((double) i + (double) j )/ 2.0;
	}
}
