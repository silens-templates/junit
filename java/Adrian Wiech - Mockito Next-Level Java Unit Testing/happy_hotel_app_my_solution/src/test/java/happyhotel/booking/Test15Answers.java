package happyhotel.booking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
class Test15Answers {

  @InjectMocks
  private BookingService bookingService;

  @Mock
  private PaymentService paymentServiceMock;
  @Mock
  private RoomService roomServiceMock;
  @Spy
  private BookingDAO bookingDAOMock;
  @Mock
  private MailSender mailSenderMock;
  @Captor
  private ArgumentCaptor<Double> doubleCaptor;

  @Test
  @DisplayName("Test 15: Answers")
  void should_CalculateCorrectPrice() {
    try (MockedStatic<CurrencyConverter> mockedConverter = mockStatic(CurrencyConverter.class)) {
      // given
      BookingRequest bookingRequest = new BookingRequest(
          "1",
          LocalDate.of(2020, 1, 1),
          LocalDate.of(2020, 1, 5),
          2,
          false
      );

      double expected = 400.0 * .8;

      mockedConverter.when(() -> CurrencyConverter.toEuro(anyDouble())).thenAnswer(inv -> (double) inv.getArgument(0) * .8);

      // when
      double actual = bookingService.calculatePriceEuro(bookingRequest);

      //then
      assertEquals(expected, actual);
    }
  }
}