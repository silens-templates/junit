package happyhotel.booking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


class Test09MockingVoidMethods {

  private BookingService bookingService;

  private PaymentService paymentServiceMock;
  private RoomService roomServiceMock;
  private BookingDAO bookingDAOMock;
  private MailSender mailSenderMock;

  @BeforeEach
  void setup() {
    paymentServiceMock = mock(PaymentService.class);
    roomServiceMock = mock(RoomService.class);
    bookingDAOMock = mock(BookingDAO.class);
    mailSenderMock = mock(MailSender.class);

    bookingService = new BookingService(
        paymentServiceMock,
        roomServiceMock,
        bookingDAOMock,
        mailSenderMock
    );
  }

  @Test
  @DisplayName("Test 09: Mocking Void Methods.")
  void should_ThrowException_When_MailNotReady() {
    // given
    BookingRequest bookingRequest = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 5),
        2,
        false
    );

    doThrow(new BusinessException()).when(mailSenderMock).sendBookingConfirmation(any());

    // when
    Executable executable = () -> bookingService.makeBooking(bookingRequest);

    // then
    assertThrows(BusinessException.class, executable);
  }

  @Test
  @DisplayName("Test 09: Mocking Void Methods.")
  void should_NotThrowException_When_MailNotReady() {
    // given
    BookingRequest bookingRequest = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 5),
        2,
        false
    );

    doNothing().when(mailSenderMock).sendBookingConfirmation(any());

    // when
    bookingService.makeBooking(bookingRequest);

    // then
    // no exception thrown
  }
}