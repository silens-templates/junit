package happyhotel.booking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
class Test13StrictStubbing {

  @InjectMocks
  private BookingService bookingService;

  @Mock
  private PaymentService paymentServiceMock;
  @Mock
  private RoomService roomServiceMock;
  @Spy
  private BookingDAO bookingDAOMock;
  @Mock
  private MailSender mailSenderMock;
  @Captor
  private ArgumentCaptor<Double> doubleCaptor;

  @Test
  @DisplayName("Test 13: Strict Stubbing")
  void should_NoExceptionThrown_when_Prepaid() {
    // given
    BookingRequest bookingRequest = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 5),
        2,
        true
    );

    // when
    bookingService.makeBooking(bookingRequest);

    //then
    // No exception is thrown
  }

  @Test
  @DisplayName("Test 13: Strict Stubbing")
  void should_InvokePayment_when_Prepaid() {
    // given
    BookingRequest bookingRequest = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 5),
        2,
        false
    );

    // add Lenient in front of unnecessary stubbing  when strict stubbing is enforced by @ExtendWith(MockitoExtension.class)
    lenient().when(paymentServiceMock.pay(any(), anyDouble())).thenReturn("1");

    // when
    bookingService.makeBooking(bookingRequest);

    //then
    // No exception is thrown
  }
}