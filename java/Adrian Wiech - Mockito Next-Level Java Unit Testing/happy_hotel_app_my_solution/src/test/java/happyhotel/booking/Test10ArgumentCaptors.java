package happyhotel.booking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class Test10ArgumentCaptors {

  private BookingService bookingService;

  private PaymentService paymentServiceMock;
  private RoomService roomServiceMock;
  private BookingDAO bookingDAOMock;
  private MailSender mailSenderMock;

  private ArgumentCaptor<Double> doubleCaptor;

  @BeforeEach
  void setup() {
    paymentServiceMock = mock(PaymentService.class);
    roomServiceMock = mock(RoomService.class);
    bookingDAOMock = mock(BookingDAO.class);
    mailSenderMock = mock(MailSender.class);

    bookingService = new BookingService(
        paymentServiceMock,
        roomServiceMock,
        bookingDAOMock,
        mailSenderMock
    );

    this.doubleCaptor = ArgumentCaptor.forClass(Double.class);
  }

  @Test
  @DisplayName("Test 10: Argument Captors")
  void should_PayCorrectPrice_When_InputOk() {
    // given
    BookingRequest bookingRequest = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 5),
        2,
        true
    );

    // when
    bookingService.makeBooking(bookingRequest);

    //then
    verify(paymentServiceMock, times(1)).pay(eq(bookingRequest), doubleCaptor.capture());
    double capturedArgument = doubleCaptor.getValue();

    assertEquals(400.0, capturedArgument);
  }

  @Test
  @DisplayName("Test 10: Argument Captors")
  void should_PayCorrectPrice_When_MultipleCalls() {
    // given
    BookingRequest bookingRequest = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 5),
        2,
        true
    );

    BookingRequest bookingRequest2 = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 2),
        2,
        true
    );

    List<Double> expected = Arrays.asList(400.0, 100.0);

    // when
    bookingService.makeBooking(bookingRequest);
    bookingService.makeBooking(bookingRequest2);

    //then
    verify(paymentServiceMock, times(2)).pay(any(), doubleCaptor.capture());
    List<Double> capturedArgument = doubleCaptor.getAllValues();

    assertEquals(expected, capturedArgument);
  }
}