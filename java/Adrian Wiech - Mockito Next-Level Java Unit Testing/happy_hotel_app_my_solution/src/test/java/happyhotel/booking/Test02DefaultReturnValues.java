package happyhotel.booking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

class Test02DefaultReturnValues {

  private BookingService bookingService;

  private PaymentService paymentServiceMock;
  private RoomService roomServiceMock;
  private BookingDAO bookingDAOMock;
  private MailSender mailSenderMock;

  @BeforeEach
  void setup() {
    paymentServiceMock = mock(PaymentService.class);
    roomServiceMock = mock(RoomService.class);
    bookingDAOMock = mock(BookingDAO.class);
    mailSenderMock = mock(MailSender.class);

    bookingService = new BookingService(
        paymentServiceMock,
        roomServiceMock,
        bookingDAOMock,
        mailSenderMock
    );

    // Default mock return values
    System.out.println("roomServiceMock.getAvailableRooms()       = " + roomServiceMock.getAvailableRooms());
    System.out.println("roomServiceMock.findAvailableRoomId(null) = " + roomServiceMock.findAvailableRoomId(null));
    System.out.println("roomServiceMock.getRoomCount()            = " + roomServiceMock.getRoomCount());
  }

  @Test
  @DisplayName("Test 02: Default Return Values")
  void should_CountAvailablePlaces() {
    // given
    int expected = 0;

    // when
    int actual = bookingService.getAvailablePlaceCount();

    // then
    assertEquals(expected, actual);
  }
}