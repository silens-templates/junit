package happyhotel.booking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.*;

class Test06Matcher {

  private BookingService bookingService;

  private PaymentService paymentServiceMock;
  private RoomService roomServiceMock;
  private BookingDAO bookingDAOMock;
  private MailSender mailSenderMock;

  @BeforeEach
  void setup() {
    paymentServiceMock = mock(PaymentService.class);
    roomServiceMock = mock(RoomService.class);
    bookingDAOMock = mock(BookingDAO.class);
    mailSenderMock = mock(MailSender.class);

    bookingService = new BookingService(
        paymentServiceMock,
        roomServiceMock,
        bookingDAOMock,
        mailSenderMock
    );
  }

  @Test
  @DisplayName("Test 05: Throwing Exceptions.")
  void should_NotCompleteBooking_When_PriceTooHigh() {
    // given
    BookingRequest bookingRequest = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 5),
        2,
        true
    );

    when(this.paymentServiceMock.pay(any(), anyDouble())).thenThrow(BusinessException.class);


    // when
    Executable executable = () -> bookingService.makeBooking(bookingRequest);

    // then
    assertThrows(BusinessException.class, executable);
  }

  @Test
  @DisplayName("Test 05: Throwing Exceptions.")
  void should_NotCompleteBooking_When_PriceIs400() {
    // given
    BookingRequest bookingRequest = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 5),
        2,
        true
    );

    when(this.paymentServiceMock.pay(any(), eq(400.0))).thenThrow(BusinessException.class);


    // when
    Executable executable = () -> bookingService.makeBooking(bookingRequest);

    // then
    assertThrows(BusinessException.class, executable);
  }
}