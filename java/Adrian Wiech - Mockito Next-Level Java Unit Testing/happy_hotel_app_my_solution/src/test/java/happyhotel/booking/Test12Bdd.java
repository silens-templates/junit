package happyhotel.booking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
class Test12Bdd {

  @InjectMocks
  private BookingService bookingService;

  @Mock
  private PaymentService paymentServiceMock;
  @Mock
  private RoomService roomServiceMock;
  @Spy
  private BookingDAO bookingDAOMock;
  @Mock
  private MailSender mailSenderMock;
  @Captor
  private ArgumentCaptor<Double> doubleCaptor;


  @Test
  @DisplayName("Test 12: Return Custom Value 2.")
  void should_CountAvailablePlaces_When_OneRoomAvailable() {
    // given
    given(this.roomServiceMock.getAvailableRooms())
        .willReturn(Collections.singletonList(new Room("Room1", 2)));

    int expected = 2;

    // when
    int actual = bookingService.getAvailablePlaceCount();

    // then
    assertEquals(expected, actual);
  }

  @Test
  @DisplayName("Test 07: Verifying Behavior")
  void should_InvokePayment_when_Prepaid() {
    // given
    BookingRequest bookingRequest = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 5),
        2,
        true
    );

    // when
    bookingService.makeBooking(bookingRequest);

    //then
    then(paymentServiceMock).should(times(1)).pay(bookingRequest, 400.0);
    verifyNoMoreInteractions(paymentServiceMock);
  }

}