package happyhotel.booking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class Test16FinalMethods {

  private BookingService bookingService;

  private PaymentService paymentServiceMock;
  private RoomService roomServiceMock;
  private BookingDAO bookingDAOMock;
  private MailSender mailSenderMock;

  @BeforeEach
  void setup() {
    paymentServiceMock = mock(PaymentService.class);
    roomServiceMock = mock(RoomService.class);
    bookingDAOMock = mock(BookingDAO.class);
    mailSenderMock = mock(MailSender.class);

    bookingService = new BookingService(
        paymentServiceMock,
        roomServiceMock,
        bookingDAOMock,
        mailSenderMock
    );
  }

  @Test
  @DisplayName("Test 16: Final Methods.")
  void should_CountAvailablePlaces_When_OneRoomAvailable() {
    // given
    when(this.roomServiceMock.getAvailableRooms())
        .thenReturn(Collections.singletonList(new Room("Room1", 2)));

    int expected = 2;

    // when
    int actual = bookingService.getAvailablePlaceCount();

    // then
    assertEquals(expected, actual);
  }

  @Test
  @DisplayName("Test 16: Final Methods.")
  void should_CountAvailablePlaces_When_MultipleRoomsAvailable() {
    // given
    List<Room> rooms = Arrays.asList(new Room("Room1", 2), new Room("Room2", 5));

    when(this.roomServiceMock.getAvailableRooms())
        .thenReturn(rooms);

    int expected = 7;

    // when
    int actual = bookingService.getAvailablePlaceCount();

    // then
    assertEquals(expected, actual);
  }
}
