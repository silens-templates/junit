package happyhotel.booking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class Test07VerifyingBehavior {

  private BookingService bookingService;

  private PaymentService paymentServiceMock;
  private RoomService roomServiceMock;
  private BookingDAO bookingDAOMock;
  private MailSender mailSenderMock;

  @BeforeEach
  void setup() {
    paymentServiceMock = mock(PaymentService.class);
    roomServiceMock = mock(RoomService.class);
    bookingDAOMock = mock(BookingDAO.class);
    mailSenderMock = mock(MailSender.class);

    bookingService = new BookingService(
        paymentServiceMock,
        roomServiceMock,
        bookingDAOMock,
        mailSenderMock
    );
  }

  @Test
  @DisplayName("Test 07: Verifying Behavior")
  void should_InvokePayment_when_Prepaid() {
    // given
    BookingRequest bookingRequest = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 5),
        2,
        true
    );

    // when
    bookingService.makeBooking(bookingRequest);

    //then
    verify(paymentServiceMock).pay(bookingRequest, 400.0);
    verifyNoMoreInteractions(paymentServiceMock);
  }

  @Test
  @DisplayName("Test 07: Verifying Behavior")
  void should_InvokePayment_when_PayCalledOnce() {
    // given
    BookingRequest bookingRequest = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 5),
        2,
        true
    );

    // when
    bookingService.makeBooking(bookingRequest);

    //then
    verify(paymentServiceMock, times(1)).pay(bookingRequest, 400.0);
    verifyNoMoreInteractions(paymentServiceMock);
  }

  @Test
  @DisplayName("Test 07: Verifying Behavior")
  void should_NotInvokePayment_when_NotPrepaid() {
    // given
    BookingRequest bookingRequest = new BookingRequest(
        "1",
        LocalDate.of(2020, 1, 1),
        LocalDate.of(2020, 1, 5),
        2,
        false
    );

    // when
    bookingService.makeBooking(bookingRequest);

    //then
    verify(paymentServiceMock, never()).pay(any(), anyDouble());
  }
}